package ee.clubstravel.club.controller;

import ee.clubstravel.club.domain.Contract;
import ee.clubstravel.club.repo.ContractRepo;
import ee.clubstravel.club.service.ContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.Map;

@Controller
public class MainController {
    private final ContractRepo contractRepo;
    private final ContractService contractService;

    private Iterable<Contract> contracts;

    @Autowired
    public MainController(ContractRepo contractRepo, ContractService contractService) {
        this.contractRepo = contractRepo;
        this.contractService = contractService;
    }

    @GetMapping("/")
    public String mainPage(){
        return "redirect:/login";
    }

    @GetMapping("/club")
    public String club(
            @RequestParam(required = false, defaultValue = "") String name,
            @RequestParam(required = false, defaultValue = "") String phone,
            @RequestParam(required = false, defaultValue = "") String contractNumber,
            Model model
    ){
        contracts = contractService.findContracts(name, phone, contractNumber);
        model.addAttribute("contracts", contracts);
        model.addAttribute("name",name);
        model.addAttribute("phone",phone);
        model.addAttribute("contractNumber",contractNumber);
        return "index";
    }

    @GetMapping("/club/create")
    public String createContractShow(){
        return "addNewContract";
    }


    @PostMapping("/club/create")
    public String addNewContract(
            @Valid Contract contract,
            BindingResult bindingResult,
            Model model
    ){
        if(contract.getStartDate() != null || contract.getEndDate() != null){
            if(contract.getStartDate().compareTo(contract.getEndDate()) > 0){
                model.addAttribute("endDateError", "End date can't be earlier than apply date!");
                model.addAttribute("contract", contract);
                return "addNewContract";
            }
        }

        if (contractRepo.findByContractNumber(contract.getContractNumber()).size() !=0 ){
            model.addAttribute("contractNumberError", "This contract already exists");
            model.addAttribute("contract",contract);
            return "addNewContract";
        }
        if(bindingResult.hasErrors()) {
            Map<String, String> errors = ControllerUtils.getErrors(bindingResult);

            model.mergeAttributes(errors);
            model.addAttribute("contract", contract);
            return "addNewContract";
        }

        if (contract.getWeeks().equals("1 week")){
            contract.setInstallmentPlan(false);
        }
        contract.setCreationDate(LocalDate.now());

        contractRepo.save(contract);
        return "redirect:/club/"+contract.getId();
    }
}
