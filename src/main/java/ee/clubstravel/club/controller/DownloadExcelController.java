package ee.clubstravel.club.controller;

import ee.clubstravel.club.domain.Contract;
import ee.clubstravel.club.repo.ContractRepo;
import ee.clubstravel.club.service.ExcelFileExporter;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Controller
@PreAuthorize("hasAuthority('ADMIN')")
public class DownloadExcelController {

    private final ContractRepo contractRepo;
    private List<Contract> contracts;

    @Autowired
    public DownloadExcelController(ContractRepo contractRepo) {
        this.contractRepo = contractRepo;
    }

    @GetMapping("/club/emails")
    public String excel(
            @RequestParam (required = false) String date,
            Model model

    ){
        if(!StringUtils.isEmpty(date)){
            Pattern pattern = Pattern.compile("-");
            Matcher matcher = pattern.matcher(date);
            if (matcher.find()) {
                try {
                    int year = Integer.parseInt(date.substring(0, matcher.start()));
                    int month = Integer.parseInt(date.substring(matcher.end()));
                    contracts = contractRepo.findByDate(month,year);
                    model.addAttribute("date",date);
                    model.addAttribute("contracts",contracts);
                    return "emails";
                }catch (NumberFormatException e){
                    System.out.println("Wrong input");
                    model.addAttribute("error","Write the correct date");
                    model.addAttribute("contracts", contracts);
                }
            }
        }
        contracts = contractRepo.findAll(Sort.by(Sort.Direction.DESC,"id"));
        model.addAttribute("contracts", contracts);
        return "emails";
    }


    @PostMapping("club/emails")
    public void downloadCsv(
            @RequestParam (required = false) String date,
            HttpServletResponse response
    )throws IOException{
        if(!StringUtils.isEmpty(date)){
            Pattern pattern = Pattern.compile("-");
            Matcher matcher = pattern.matcher(date);
            if (matcher.find()) {
                int year = Integer.parseInt(date.substring(0, matcher.start()));
                int month = Integer.parseInt(date.substring(matcher.end()));
                contracts = contractRepo.findByDate(month,year);
                response.setContentType("application/octet-stream");
                response.setHeader("Content-Disposition", "attachment; filename=club_emails_"+date+".xlsx");
                ByteArrayInputStream stream = ExcelFileExporter.contractEmailsToExcel(contracts);
                IOUtils.copy(stream,response.getOutputStream());
            }
        }
    }


}
