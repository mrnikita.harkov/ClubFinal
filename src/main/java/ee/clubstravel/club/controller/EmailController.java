package ee.clubstravel.club.controller;

import ee.clubstravel.club.domain.Contract;
import ee.clubstravel.club.repo.ContractRepo;
import ee.clubstravel.club.service.EmailService;
import org.apache.commons.collections4.IterableUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import static java.time.temporal.ChronoUnit.DAYS;

@Controller
public class EmailController {
    private final ContractRepo contractRepo;
    private final EmailService emailService;

    @Autowired
    public EmailController(ContractRepo contractRepo, EmailService emailService) {
        this.contractRepo = contractRepo;
        this.emailService = emailService;
    }

    //@Scheduled(cron = "0 30 15 * * ?") //Every day at 15:30PM
    //@Scheduled(cron = "*/15 * * * * ?")
    public void sendEmails(){
        Iterable<Contract> contracts = contractRepo.findByInstallmentPlan(true);
        LocalDate today = LocalDate.now();
        int days = 3;
        System.out.println(" SendEmails method started at: " + new Date()+"\nCount of contracts: "+ IterableUtils.size(contracts));
        for (Contract contract: contracts) {
            try {
                if (contract.getFirstPayment()!=null) {
                    if (getFirstDays(today, contract) == days) {
                        messageTemplate(contract, contract.getFirstPayment(),contract.getFirstSum());
                        if(contract.getSecondPayment()==null) {
                            contract.setInstallmentPlan(false);
                        }
                    }
                }if (contract.getSecondPayment() != null) {
                    if (getSecondDays(today, contract) == days) {
                        messageTemplate(contract, contract.getSecondPayment(), contract.getSecondSum());
                        if(contract.getThirdPayment()==null){
                            contract.setInstallmentPlan(false);
                        }
                    }
                }if(contract.getThirdPayment()!=null){
                    if (getThirdDays(today, contract) == days) {
                        messageTemplate(contract, contract.getThirdPayment(),contract.getThirdSum());
                        contract.setInstallmentPlan(false);
                    }
                }

            }catch (Exception e){
                System.out.println(e);
            }
        }
    }

    private long getFirstDays(LocalDate today, Contract contract) {
        return DAYS.between(today, contract.getFirstPayment());
    }

    private long getSecondDays(LocalDate today, Contract contract) {
        return DAYS.between(today, contract.getSecondPayment());
    }
    private long getThirdDays(LocalDate today, Contract contract) {
        return DAYS.between(today, contract.getThirdPayment());
    }

    private void messageTemplate(Contract contract, LocalDate date, String sum){
        String message = String.format(
                "<body>\n" +
                        "<p>Hello, <h4>%s!</h4></p>\n" +
                        "<p>This is a test message, to explore the scheduler method.</p.\n" +
                        "<p><span>Your contract ends in: %s </span></p>\n" +
                        "<p>So hurry up, pack your bags, and fly away!</p>\n" +
                        "<h3>%s</h3>" +
                        "You need to pay for <i>%s</i>: %s eur\n" +
                        "<footer>" +
                            "<img src='https://clubstravel.ee/wp-content/uploads/2019/03/logo-2.png'" +
                        "</footer>" +
                "</body>",
                contract.getName(),
                contract.getEndDate().format(DateTimeFormatter.ofPattern("dd-MM-yyyy")),
                contract.getContractNumber(),
                date.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")),
                sum
        );
        emailService.sendEmail(contract.getEmail(), "Payment Clubs Travel", message);//Send email constructor
        System.out.println("New message for "+contract.getName()+" was sent at:\n"+new Date() +" for: "+contract.getEmail() );

    }
}


