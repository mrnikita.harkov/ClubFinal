package ee.clubstravel.club.domain;

import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Table
public class Contract {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Length(min = 5,max = 100, message = "Insert name from 5 to 100 characters")
    private String name;
    @NotBlank(message = "Field cannot be empty!")
    @Length(max = 11, message = "Maximum 11 characters!")
    private String personalCode;
    @NotBlank(message = "Phone cannot be empty!")
    private String phone;
    @Email(message = "Email is not correct!")
    @NotBlank(message = "Email cannot be empty!")
    private String email;

    @NotBlank(message = "Contract number cannot be empty!")
    private String contractNumber;

    private String weeks;

    @NotNull(message = "Please insert date!")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate startDate;
    @NotNull(message = "Please insert date!")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate endDate;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate firstPayment;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate secondPayment;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate thirdPayment;

    private String firstSum;
    private String secondSum;
    private String thirdSum;


    @Column(columnDefinition = "boolean default false")
    private boolean installmentPlan;

    private LocalDate creationDate;
    private boolean trip;
    @Length(max = 2048)
    private String comment;

    public Contract() {
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPersonalCode() {
        return personalCode;
    }

    public void setPersonalCode(String personalCode) {
        this.personalCode = personalCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getWeeks() {
        return weeks;
    }

    public void setWeeks(String weeks) {
        this.weeks = weeks;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public boolean isTrip() {
        return trip;
    }

    public void setTrip(boolean trip) {
        this.trip = trip;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDate getFirstPayment() {
        return firstPayment;
    }

    public void setFirstPayment(LocalDate firstPayment) {
        this.firstPayment = firstPayment;
    }

    public LocalDate getSecondPayment() {
        return secondPayment;
    }

    public void setSecondPayment(LocalDate secondPayment) {
        this.secondPayment = secondPayment;
    }

    public LocalDate getThirdPayment() {
        return thirdPayment;
    }

    public void setThirdPayment(LocalDate thirdPayment) {
        this.thirdPayment = thirdPayment;
    }

    public boolean isInstallmentPlan() {
        return installmentPlan;
    }

    public void setInstallmentPlan(boolean installmentPlan) {
        this.installmentPlan = installmentPlan;
    }

    public String getFirstSum() {
        return firstSum;
    }

    public void setFirstSum(String firstSum) {
        this.firstSum = firstSum;
    }

    public String getSecondSum() {
        return secondSum;
    }

    public void setSecondSum(String secondSum) {
        this.secondSum = secondSum;
    }

    public String getThirdSum() {
        return thirdSum;
    }

    public void setThirdSum(String thirdSum) {
        this.thirdSum = thirdSum;
    }
}
