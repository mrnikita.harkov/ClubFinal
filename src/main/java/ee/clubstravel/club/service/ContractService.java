package ee.clubstravel.club.service;

import ee.clubstravel.club.domain.Contract;
import ee.clubstravel.club.repo.ContractRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class ContractService {
    private Iterable<Contract> contracts;
    private final ContractRepo contractRepo;

    @Autowired
    public ContractService(ContractRepo contractRepo) {
        this.contractRepo = contractRepo;
    }

    public Iterable<Contract> findContracts(String name, String phone, String contractNumber){
            if(name!=null && !name.isEmpty()){
                contracts = contractRepo.findByFilter(name,phone,contractNumber);
            }else if(phone!=null && !phone.isEmpty()){
                contracts = contractRepo.findByFilter(name,phone,contractNumber);
            }else if(contractNumber!=null && !contractNumber.isEmpty()){
                contracts = contractRepo.findByFilter(name,phone,contractNumber);
            }else{
                contracts = contractRepo.findAll(Sort.by(Sort.Direction.DESC,"id"));
            }
        return contracts;
    }
}
