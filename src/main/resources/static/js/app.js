document.ondragstart = noselect;
document.onselectstart = noselect;
document.oncontextmenu = noselect;
function noselect() {return false;}

// Restricts input for the given textbox to the given inputFilter.
function setInputFilter(textbox, inputFilter) {
  ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
    textbox.addEventListener(event, function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      } else {
        this.value = "";
      }
    });
  });
}

setInputFilter(document.getElementById("firstSum"), function(value) {
  return /^-?\d*[.,]?\d*$/.test(value);
  });

setInputFilter(document.getElementById("secondSum"), function(value) {
    return /^-?\d*[.,]?\d*$/.test(value);
  });

setInputFilter(document.getElementById("thirdSum"), function(value) {
      return /^-?\d*[.,]?\d*$/.test(value);
  });
